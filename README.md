[![Awesome](https://awesome.re/badge.svg)](https://awesome.re)

Collection of awesome projects on LBRY.

## Clients
- [FastLBRY](https://notabug.org/jyamihud/FastLBRY-terminal) <br>
     A fully featured, terminal application to interact with LBRY. It will allow watching videos, download files, view and send comments, upload new files. This stage is half finished.
- [LBRY Desktop](https://github.com/lbryio/lbry-desktop) <br>
     Official LBRY desktop client based on Electron.
- [lbt](https://gitlab.com/gardenappl/lbt) <br>
     lbt is a collection of command-line tools for interacting with the LBRY network, written in POSIX shell.

## Applications
- [Hound.fm](https://hound.fm/music/latest) [(git)](https://github.com/Hound-fm) <br>
     Audio content agregator for LBRY.

## Scripts & one-purpose applications
- [lbry-sync-ytdl](https://gitlab.com/gardenappl/lbry-sync-ytdl) <br>
     lbry-sync-ytdl is a shell script for uploading content to LBRY using youtube-dl.
- [lbry-channel-feed](https://gitlab.melroy.org/melroy/lbry-channel-feed) <br>
     LBRY RSS Feed provider 🗞 - Support channels at the moment (RSS, Atom & JSON).
- [LBRY Utility Scripts](https://odysee.com/$/list/3a8c64f781ab2ed2d17f8f808c708a5ee0b04423) <br>
     Various python scripts for LBRY by Brendon Brewer. <br>
     - A Python script to download all content (or just recent content) from a single LBRY channel
     - A Python script to check all your LBRY vanity names (LBRY Desktop)
     - A Python tool to manage stalled LBRY downloads
     - A Python script to delete files from your LBRY Library on a per-channel basis
     - Estimate your LBRY seeding ratio with this little Python script
- [LBRYFileUploader](https://github.com/Blanxs/LBRYFileUploader) <br>
     Java based mass file uploader for LBRY.
- [lbry_discord_repost_bot](https://github.com/neofutur/lbry_discord_repost_bot) <br>
     Discord bot to post new uploads on a LBRY channel to a specific discord channel.

## Browser extensions
- Watch on Odysee [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/watch-on-odysee/) [Chromium](https://chrome.google.com/webstore/detail/watch-on-odysee/kofmhmemalhemmpkfjhjfkkhifonoann) [GitHub](https://github.com/kodxana/Watch-on-Odysee) <br>
     Plugin that automatically redirects you to the Odysee version of any video on YouTube that is available on Odysee.
- Watch on LBRY [Firefox](https://addons.mozilla.org/en-US/firefox/addon/watch-on-lbry/) [Chromium](https://chrome.google.com/webstore/detail/watch-on-lbry/jjmbbhopnjdjnpceiecihldbhibchgek) [GitHub](https://github.com/LBRYFoundation/Watch-on-LBRY) <br>
     Plug that automatically redirects you to the LBRY network when it detects that the YouTube video is there.
- LBRY Link [Chromium](https://chrome.google.com/webstore/detail/lbry-link/bnhpdmdbfbnopgncbpgdkidpnmkbidfa) [GitHub](https://github.com/seanyesmunt/lbry-link) <br>
     Makes `lbry://` links clickable.
- LBC Today [Firefox](https://addons.mozilla.org/en-GB/firefox/addon/lbc/) [Chromium](https://chrome.google.com/webstore/detail/lbc-today/ealgadmpgaefckfpclemccenfkjihedn) [GitHub](https://github.com/VladHZC/lbc-today/) <br>
     Check LBC price live on your browser.

## Libraries & API's
- [LBRY SDK (lbrynet)](https://github.com/lbryio/lbry-sdk) <br>
     LBRY SDK for Python is currently the most fully featured implementation of the LBRY Network protocols and includes many useful components and tools for building decentralized applications.
- [lbry-trending-algos](https://github.com/eggplantbren/lbry-trending-algos) <br>
     Various LBRY trending algorithms by Brendon Brewer.
- [lbrytools](https://github.com/belikor/lbrytools) <br>
     A library of functions that can be used to manage the download of claims from the LBRY network. It includes methods to download claims by URI (canonical url), claim ID, or from specific channels.